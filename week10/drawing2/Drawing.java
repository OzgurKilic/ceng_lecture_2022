package drawing2;

import java.util.ArrayList;

public class Drawing {

    ArrayList<Object> shapes = new ArrayList<Object>();

    public double calculateTotalArea(){
        double totalArea = 0;

        for (Object shape : shapes){
            //totalArea += shape.area();

            if(shape instanceof  Circle)
                totalArea += ((Circle)shape).area();    // totalArea = totalArea + circle.area();
            else if(shape instanceof Rectangle)
                totalArea += ((Rectangle)shape).area();
        }


        return totalArea;
    }

    public void addShape(Object shape) {
        shapes.add(shape);
    }


}
