import shape.Box;
import shape.Rectangle;
import shape.Shape;
import shape.Circle;

public class AbstractDemo {

    public static void main(String[] args){
        Shape[] shapes = new Shape[3];
        shapes[0] =  new Circle(5, "Yellow");////"Hello";            //new Shape("Green");
        shapes[1] =  new Rectangle("Yellow",5,6);
        shapes[2] = new Box("Green",4,5,6);

        for (Shape s : shapes) {
            s.area();

        }

        //Shape s = shapes[2];
        //System.out.println(s.getClass().getName());
    }
}
