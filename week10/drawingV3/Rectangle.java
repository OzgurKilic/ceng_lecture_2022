package drawingV3;

public class Rectangle extends Shape{

    int width;
    int length;

    @Override
    public double area(){
        return length * width;
    }

    public Rectangle(int width, int length) {
        this.width = width;
        this.length = length;
    }
}
