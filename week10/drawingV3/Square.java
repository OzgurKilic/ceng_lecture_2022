package drawingV3;

public class Square extends Shape{

    int side;

    @Override
    public double area(){
        return side * side;
    }

    public Square(int side) {
        this.side = side;
    }
}
