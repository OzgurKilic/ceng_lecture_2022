package drawingV3;

public class Circle extends Shape{

    int radius;

   @Override
    public double area(){
        return Math.PI * radius* radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }
}
