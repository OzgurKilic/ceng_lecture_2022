import shape.Box;
import shape.Rectangle;
import shape.Shape;
import shape.Circle;

public class CastingDemo {

    public static void main(String[] args){
        Box box = new Box("Yellow",4,5,6);
        Rectangle r = box;
        Shape s = box;
        Object o = box;

        o.equals("Hello");

        box.area();
        ///s.area();
       // o.area();

        if(o  instanceof Box)
            ((Box) o).area();
        if(o instanceof String)
            ((String) o).toString();


        if(o  instanceof Box) {
            Box b1 = (Box) o;
            b1.area();
        }

        System.out.println(o==box);


        System.out.println(box.equals(new Box("Yellow",4,5,6)));;
        System.out.println(box.equals(new Box("Green",4,5,6)));;
        System.out.println(box.equals("Hello"));

        Circle c = new Circle(5, "Green");
        System.out.println(c.area());


        //Shape shape = new Shape();



    }
}
