package drawingV1;

import java.util.ArrayList;

public class Drawing {

    ArrayList<Circle> circles = new ArrayList<Circle>();
    ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();

    public double calculateTotalArea(){
        double totalArea = 0;

        for (Circle circle : circles){
            totalArea += circle.area();    // totalArea = totalArea + circle.area();
        }

        for (Rectangle rect : rectangles){
            totalArea += rect.area();     // totalArea = totalArea + rect.area();
        }
        return totalArea;
    }

    public void addShape(Circle circle) {
        circles.add(circle);
    }

    public void addShape(Rectangle rectangle) {
        rectangles.add(rectangle);
    }
}
