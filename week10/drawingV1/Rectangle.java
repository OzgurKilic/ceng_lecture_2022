package drawingV1;

public class Rectangle {

    int width;
    int length;

    public double area(){
        return length * width;
    }

    public Rectangle(int width, int length) {
        this.width = width;
        this.length = length;
    }
}
