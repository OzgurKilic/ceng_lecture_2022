package drawingV1;

public class TestDrawing {

    public static void main(String[] args){
        Drawing drawing = new Drawing();

        drawing.addShape(new Circle(5));
        drawing.addShape(new Circle(7));

        drawing.addShape(new Rectangle(5,6));
        drawing.addShape(new Rectangle(3,8));

        System.out.println(drawing.calculateTotalArea());
    }
}
