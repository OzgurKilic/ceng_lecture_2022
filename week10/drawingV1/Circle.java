package drawingV1;

public class Circle {

    int radius;

    public double area(){
        return Math.PI * radius* radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }
}
