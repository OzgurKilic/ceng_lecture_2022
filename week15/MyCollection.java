public class MyCollection<T extends Number> {

    Box<T> head;


    public Box<T> getHead() {
        return head;
    }


    public void add(T value) {
        if (head == null)
            head = new Box<>(value);
        else {
            Box<T> tail = getTail();
            tail.setNext(new Box<>(value));
        }
    }

    public int size(){
        return countItems(head);
    }

    private int countItems(Box<T> head) {
        if (head == null)
            return  0;
        return 1 + countItems(head.getNext());
    }

    private Box<T> getTail() {
        if (head == null)
            return null;
        Box<T> tail = head;
        while (tail.getNext() != null)
            tail = tail.getNext();
        return tail;
    }

    public T getLastValue(){
        return getTail().getValue();
    }

    public void addAll(MyCollection<? extends T> longMyCollection) {
        Box<? extends T> start = longMyCollection.head;
        getTail().setNext(new Box<T>(start.getValue()));
        while (start.getNext() != null) {
            start = start.getNext();
            getTail().setNext(new Box<T>(start.getValue()));
        }
    }
}
