public class Box<N extends Number> {



    private N value;

    Box<N> next;

    public Box(N value) {
        this.value = value;
    }

    public N getValue() {
        return value;
    }

    public void setValue(N value) {
        this.value = value;
    }

    public Box<N> getNext() {
        return next;
    }

    public void setNext(Box<N> next) {
        this.next = next;
    }
}
