import shape.Circle;
import shape.Shape;
import shape.Rectangle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGenerics {
    public static void main(String[] args){

        List<String> strList = new ArrayList<>();

        //strList.add(9);
        strList.add("Halo");
        strList.addAll(new ArrayList<>());
        String str = strList.get(0);

        //Box<String> box = new Box<>("Hallo");
        //box.setValue(5);
        //String value = box.getValue();

        //box.setNext(new Box<>("Hi"));
        //box.getNext().setNext(new Box<>("Merhaba"));


        Box<Integer> box2 = new Box<>(9);

        box2.setNext(new Box<>(5));

        //Box<Circle> box3 = new Box<>(new Circle(5,"blue"));
        //box3.setNext(new Box<> (new Circle(4, "yellow")));

        //Box boxRaw = new Box("Hello"); //Raw type

        MyCollection<Long> longMyCollection = new MyCollection<>();
        longMyCollection.add(5L);
        longMyCollection.add((long)6);
        longMyCollection.add(0L);
        longMyCollection.add(3000000000L);

        System.out.println(longMyCollection.size());
        System.out.println(longMyCollection.getLastValue());
        //mystrList.add(9);


        Map<String,Integer> dictionary = new HashMap<>();
        dictionary.put("ali",5);

        Number number = new Double(5);


        Pair<Integer,List<String>> pair = new Pair<>(5,new ArrayList<>());

        //TestGenerics.<Integer,String>compare(new Pair<>(5,""), new Pair<>(4,5));

        Number n = 5;
        System.out.println((Integer)n);

        Box<Integer> num = new Box(5);
        //Box<Number> num2 = num;

        MyCollection<Number> numberList = new MyCollection<>();
        numberList.add(5);
        numberList.add(5.5);
        numberList.add(3000000000L);

        numberList.addAll(longMyCollection);

    }


    public static <K,V> boolean compare(Pair<K,V> p1, Pair<K,V> p2){
        return p1.key.equals(p2.key);





    }

}
