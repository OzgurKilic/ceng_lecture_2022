package drawingv4;

public class Text extends A implements Drawable{

    private final String text;

    public Text(String text) {
        this.text = text;
    }

    public String getText() {

        return text;
    }


    public void draw(){
        System.out.println(text  + " is being drawn");
    }

}
