package drawingv4;




import java.util.ArrayList;

public class Drawing {

    ArrayList<Drawable> drawables = new ArrayList<>();



    public double calculateTotalArea(){
        double totalArea = 0;

        for (Drawable drawable : drawables){
            if (drawable instanceof Shape) {
                Shape shape = (Shape) drawable;
                totalArea += shape.area();
            }

        }

        return totalArea;
    }

    public void drawAll(){


        for (Drawable drawable : drawables){
            drawable.draw();

        }


    }

    public void addShape(Drawable drawable) {
        drawables.add(drawable);
    }


}
