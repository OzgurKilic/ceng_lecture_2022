package drawingv4;

import java.io.Serializable;

public  interface Drawable extends Serializable {

    void draw();
}
