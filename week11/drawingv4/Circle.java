package drawingv4;

public class Circle extends Shape {

    int radius;

   @Override
    public double area(){
        return Math.PI * radius* radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public void draw(){
        System.out.println("Circle is being drawn");
    }
}
