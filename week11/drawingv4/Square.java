package drawingv4;

public class Square extends Shape {

    int side;

    @Override
    public double area(){
        return side * side;
    }

    public Square(int side) {
        this.side = side;
    }

    public void draw(){
        System.out.println("Square is being drawn");
    }
}
