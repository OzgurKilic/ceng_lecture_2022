public class VariableExample{
	
	public static void main(String[] args){

		double dValue =  0.1;
		float value = 0.1F;

		int iValue = 5;
		long lValue = 2147483648L;

		iValue = (int)lValue;

		dValue = iValue;

		String name = "Hello";

		System.out.println(Integer.MAX_VALUE);

	}
	
}