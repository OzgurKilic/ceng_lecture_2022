package shape;

public abstract class Shape extends Object{

    protected String color;


    public Shape(String color) {
        this.color = color;
        System.out.println("Shape is being initialized with color " + color);
    }

    public abstract double area();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return
                "color='" + color + '\'' ;
    }
}
