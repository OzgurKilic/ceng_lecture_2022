package shape;

public class Box extends  Rectangle{

    int height;
    //int width;
    //String color;

    public Box(String color){
        super(color);
        System.out.println(" Initializing Box");
    }

    public Box(String color, int width, int length, int height) {
        super(color, width, length);
        this.height = height;
    }

    public double area(){
        return 2 * (super.area() + super.width * height + height *length);
    }

    void setDimension(int a, int b, int c){
        setDimension(a,b);
        height = c;
    }

    void setDimension(int a){
        super.setDimension(a);
        height = a;
    }

    public String toString() {
        return "height = " + height + ", " + super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Box){
            Box b = (Box) obj;
            return color == b.color && length == b.length
                    && width == b.width && height == b.height;
        }
        return false;
    }
}
