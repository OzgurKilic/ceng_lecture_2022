package shape;

public class Circle extends Shape{



    int radius;

    static final double PI = 3.14;


    public Circle(int radius, String color) {
        //super();
        super(color);
        this.radius = radius;
        //setColor(color);
    }

    @Override
    public double area() {
        return PI * radius * radius;
    }
}
