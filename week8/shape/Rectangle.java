package shape;

public  class Rectangle extends Shape{

    protected int width;
    protected int length;

    public Rectangle(){
        super("WHITE");


        System.out.println(" Rectangle is being inialized");

        width =9;
    }

    public Rectangle(String color){

        super(color);
        System.out.println(" Rectangle is being inialized");
        width =9;
    }

    public Rectangle(String color, int width, int length) {
        super(color);
        this.width = width;
        this.length = length;
    }


    public   double area(){
        return width * length;
    }


    void setDimension(int a){
        length = a;
        width =  a;
    }

    void setDimension(int a, int b){
        length = a;
        width =  b;
    }

    @Override
    public String toString() {
        return
                "width=" + width +
                ", length=" + length + ", "
                 + super.toString();
    }
}
