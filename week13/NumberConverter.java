import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class NumberConverter {



    Map<String,Integer> dictionary = new HashMap();

    public NumberConverter(){
        dictionary.put("zero",0);
        dictionary.put("one",1);
        dictionary.put("two",2);
    }



    public int convert(String str) throws InvalidNumberException{

            if (dictionary.get(str) == null)
                throw new InvalidNumberException(str + " is not a valid number");
            else
                return dictionary.get(str);

    }

    public String foo(String str){

        try {
            FileReader reader = new FileReader(str);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return str.toString();

    }

}
