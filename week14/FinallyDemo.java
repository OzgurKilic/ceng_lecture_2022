public class FinallyDemo {

    public static void main(String[] args) {

        try {
            foo(3);
            System.out.println("after foo call");
        }catch(IllegalArgumentException ex){
            System.out.println("recovered");
            return ;
        }finally{
            System.out.println("finally");
        }
        System.out.println("main ends");
    }

    private static void foo(int val) {

        if (val == 3)
            throw new IllegalArgumentException("3 not va valid value");
    }
}
