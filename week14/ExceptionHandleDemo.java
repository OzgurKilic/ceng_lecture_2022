public class ExceptionHandleDemo {


    public static void main(String[] args) throws AnotherException, AnException {


        try {
            System.out.println("before zoo");
            zoo(3);
            System.out.println("after zoo");
        } catch (IllegalArgumentException ex) {
            System.out.println("VAlue should not be 3");
        } catch (Exception e) {
            System.out.println("VAlue should be between 2 and 5");

        }

        System.out.println(" main ends");

    }


        private static void zoo(int value) throws AnException, AnotherException {
            try {
                System.out.println("before foo");
                foo(3);
                System.out.println("after foo");
            }catch(Exception ex ){
                System.out.println("partial recovery code");
               throw ex;
            }
            System.out.println("zoo ends");
        }

        private static void foo(int value) throws AnException, AnotherException {

        if (value < 2)
            throw new AnException();
        if (value > 5)
            throw new AnotherException();
        if (value == 3)
            throw new IllegalArgumentException();
        System.out.println("valid input");

    }
}
